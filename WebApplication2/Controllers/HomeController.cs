﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        
        {
            String dil = Request.QueryString["Language"];

            if(@dil!=null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(@dil);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(@dil);

                HttpCookie cookie = new HttpCookie("Language");
                cookie.Value = @dil;
                Response.Cookies.Add(cookie);
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult page1()
        {
            return View();
        }
    }
}